import { defineComponent, ref, renderSlot,onActivated } from "vue";
import { useScroll } from '@vueuse/core'

export default defineComponent({
    setup(props, {slots}) {
        
        const scrollRef = ref()

        const {x,y} = useScroll(scrollRef)

        onActivated(()=>{
            const beforeY = y.value
            const beforeX = x.value
            y.value = beforeY
            x.value = beforeX
        })

        return function(){
            return (
                <div ref={scrollRef}>
                    {renderSlot(slots,'default')}
                </div>
            )
        }

    },
})