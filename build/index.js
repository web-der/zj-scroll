
import { build, defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'


build(defineConfig({
    configFile: false,
    publicDir: false,
    plugins: [
        vue(),
        vueJsx(),
    ],
    build: {
        lib: {
            entry: `./src/components/ZjScroll.tsx`,
            name: `ZjScroll`,
            fileName: `zj-scroll`,
            // formats: ['umd'],
        },
        rollupOptions: {
            external: ['vue'],
            output: {
                globals: {
                    vue: "Vue"
                }
            }
        }
    }
}))
